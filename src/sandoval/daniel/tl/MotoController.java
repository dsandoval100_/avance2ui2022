package sandoval.daniel.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sandoval.daniel.bl.entities.Auto.Auto;
import sandoval.daniel.bl.entities.Moto.Moto;
import sandoval.daniel.bl.logic.MotoGestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MotoController implements Initializable {

    private Moto usuarioSesion;
    private String mensaje;
    private MotoGestor gestor = new MotoGestor();
    ObservableList<Object> motoDAO = FXCollections.observableArrayList();

    @FXML
    private Button btnActualizarMoto;

    @FXML
    private Button btnEliminarMoto;

    @FXML
    private Button btnInicio;

    @FXML
    private Button btnRegistrarMoto;

    @FXML
    private Button btnSalir;

    @FXML
    private TableColumn colMarMoto;

    @FXML
    private TableColumn colPlacaMoto;

    @FXML
    private TableColumn colTipoMoto;

    @FXML
    private TableView tblMoto;

    @FXML
    private TextField txtMarcaMoto;

    @FXML
    private TextField txtPlacaMoto;

    @FXML
    private TextField txtTipoMoto;

    @FXML
    void SALIR(ActionEvent event) throws IOException {

        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }
    @FXML
    void salir(ActionEvent event) throws IOException {

        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Usuario.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            cargarListaMotos();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    public void cargarListaMotos() throws Exception{
        tblMoto.getItems().clear();
        gestor.listarMoto().forEach(Moto -> motoDAO.add(Moto));

        colMarMoto.setCellValueFactory(new PropertyValueFactory<>("marca"));
        colPlacaMoto.setCellValueFactory(new PropertyValueFactory<>("placa"));
        colTipoMoto.setCellValueFactory(new PropertyValueFactory<>("tipo"));

        tblMoto.setItems(motoDAO);

    }

    public void cargarMoto(){
        Moto usuario = (Moto) tblMoto.getSelectionModel().getSelectedItem();
        if(usuario !=null){
            txtMarcaMoto.setText(usuario.getMarca());
            txtPlacaMoto.setText(usuario.getPlaca());
            txtTipoMoto.setText(usuario.getTipo());

        }

    }





    public void registrarMoto() throws Exception {
        mensaje = gestor.registrarMoto(txtMarcaMoto.getText(),
                txtPlacaMoto.getText(),txtTipoMoto.getText());
        cargarListaMotos();
        mostrarMensaje(mensaje);


    }



    public void actualizarMoto() throws Exception  {
        mensaje = gestor.actualizarMoto(txtMarcaMoto.getText(),
                txtPlacaMoto.getText(),txtTipoMoto.getText());
        cargarListaMotos();
        mostrarMensaje(mensaje);
    }


    public void eliminarMoto() throws Exception {
        mensaje = gestor.eliminarMoto(txtPlacaMoto.getText());
        cargarListaMotos();
        mostrarMensaje(mensaje);

    }



    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }
    public void closeWindows() {
    }
}
