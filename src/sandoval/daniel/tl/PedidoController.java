package sandoval.daniel.tl;

        import javafx.fxml.FXML;
        import javafx.scene.control.TableColumn;
        import javafx.scene.control.TableView;

public class PedidoController {

    @FXML
    private TableColumn<?, ?> id_DAT;

    @FXML
    private TableColumn<?, ?> id_DIR;

    @FXML
    private TableColumn<?, ?> id_FECH;

    @FXML
    private TableColumn<?, ?> id_ID;

    @FXML
    private TableColumn<?, ?> id_NUM;

    @FXML
    private TableColumn<?, ?> id_PAG;

    @FXML
    private TableView<?> tblPED;

    public void closeWindows() {
    }

}
