package sandoval.daniel.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.bl.entities.Usuario.Usuario;
import sandoval.daniel.bl.logic.AdministradorGestor;
import sandoval.daniel.bl.logic.ConductorGestor;
import sandoval.daniel.bl.logic.PersonaGestor;
import sandoval.daniel.bl.logic.UsuarioGestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginUsuarioController{


    private PersonaGestor gestor = new PersonaGestor();

    @FXML
    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private TextField txtUsuario;
    @FXML
    private PasswordField txtClave;

    public void validarDatos(ActionEvent event) throws Exception {
        String usuario = txtUsuario.getText();
        String clave = txtClave.getText();
        String tipoUsuario = "error";
        Persona usuarioBuscado = new Persona();
        usuarioBuscado.setUsuario(usuario);
        usuarioBuscado.setClave(clave);
        usuarioBuscado = verificarAcceso(usuarioBuscado);
        redireccionar(usuarioBuscado,event);
    }

    private Persona verificarAcceso(Persona usuarioBuscado) throws Exception {
        Persona Persona;
        Persona = gestor.verificarAcceso(usuarioBuscado);
        return Persona;
    }



    public void redireccionar(Persona usuarioBuscado, ActionEvent event) throws IOException {
        String tipoPersona = usuarioBuscado.getTipoPersona();
    System.out.println(usuarioBuscado.getNombre()+usuarioBuscado.getApellido()+usuarioBuscado.getId()+usuarioBuscado.getUsuario()+usuarioBuscado.getClave()+usuarioBuscado.getTipoPersona());
        if(tipoPersona.equals("Administrador")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/Administrador.fxml"));
            root = loader.load();
            AdministradorController index = loader.getController();
            index.setUsuarioLogueado(usuarioBuscado);
            stage =(Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setUserData(usuarioBuscado);
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }


        if(tipoPersona.equals("Conductor")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/Conductor.fxml"));
            root = loader.load();
            ConductorController index = loader.getController();
            index.setUsuarioLogueado(usuarioBuscado);
            stage =(Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

        }if(tipoPersona.equals("Usuario")){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/Usuario.fxml"));
            root = loader.load();
            UsuarioController index = loader.getController();
            index.setUsuarioLogueado(usuarioBuscado);
            stage =(Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

        }if(tipoPersona.equals("Error")) {
//            system.out.println;
        }

    }


    public void salir() {
        System.exit(0);
    }

    public void closeWindows() {
    }

    @FXML
    void SALIRR(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }
}
