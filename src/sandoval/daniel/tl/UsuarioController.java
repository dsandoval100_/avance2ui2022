package sandoval.daniel.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.bl.entities.Usuario.Usuario;
import sandoval.daniel.bl.logic.UsuarioGestor;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;


    private Usuario usuario1;
    private Usuario usuarioSesion;
    private String mensaje;
    private UsuarioGestor gestor = new UsuarioGestor();
    ObservableList<Object> usuarioDAO = FXCollections.observableArrayList();

    @FXML
    private Button btnActualizarUsuario;

    @FXML
    private Button btnEliminarUsuario;

    @FXML
    private Button btnInicio;


    @FXML
    private Button btnSalir;

    @FXML
    private TableColumn colApe;

    @FXML
    private TableColumn colAva;

    @FXML
    private TableColumn colCel;

    @FXML
    private TableColumn colCla;

    @FXML
    private TableColumn colDir;

    @FXML
    private TableColumn colFecha;

    @FXML
    private TableColumn colGen;

    @FXML
    private TableColumn colId;

    @FXML
    private TableColumn colNom;

    @FXML
    private TableColumn colUsua;
    @FXML
    private TableColumn colEdad;

    @FXML
    private TableColumn colCan;

    @FXML
    private TableColumn colDist;

    @FXML
    private TableColumn colProv;

    @FXML
    private Label lblTitulo;

    @FXML
    private TableView tblUsuario;

    @FXML
    private TextField txtApellido;

    @FXML
    private TextField txtAvatar;

    @FXML
    private TextField txtCelular;

    @FXML
    private TextField txtClave;

    @FXML
    private TextField txtDireccion;

    @FXML
    private TextField txtFecha;

    @FXML
    private TextField txtGenero;

    @FXML
    private TextField txtID;

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtEdad;

    @FXML
    private TextField txtUsuario;

    @FXML
    private TextField txtCanton;

    @FXML
    private TextField txtDistrito;

    @FXML
    private TextField txtProvincia;

    private Persona usuarioLogueado;

    private Node cerrar;




    public void setUsuarioLogueado(Persona persona) { // Setting the client-object in ClientViewController
        this.usuarioLogueado = persona;
        setTitulo();
    }

    private void setTitulo(){
        lblTitulo.setText("Bienvenido " + usuarioLogueado.getId() + " - " + usuarioLogueado.getNombre());
    }


    @FXML
    public void cargarListaUsuarios() throws Exception{

        tblUsuario.getItems().clear();
        gestor.listarUsuario().forEach(Usuario -> usuarioDAO.add(Usuario));

        colNom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colApe.setCellValueFactory(new PropertyValueFactory<>("apellido"));
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colUsua.setCellValueFactory(new PropertyValueFactory<>("usuario"));
        colCla.setCellValueFactory(new PropertyValueFactory<>("clave"));
        colDir.setCellValueFactory(new PropertyValueFactory<>("direccionExacta"));
        colProv.setCellValueFactory(new PropertyValueFactory<>("provincia"));
        colCan.setCellValueFactory(new PropertyValueFactory<>("canton"));
        colDist.setCellValueFactory(new PropertyValueFactory<>("distrito"));
        colAva.setCellValueFactory(new PropertyValueFactory<>("avatar"));
        colFecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        colEdad.setCellValueFactory(new PropertyValueFactory<>("edad"));
        colGen.setCellValueFactory(new PropertyValueFactory<>("genero"));
        colCel.setCellValueFactory(new PropertyValueFactory<>("celular"));

        tblUsuario.setItems(usuarioDAO);

    }

    public void cargarUsuario(){
         usuario1 = (Usuario) tblUsuario.getSelectionModel().getSelectedItem();
        if(usuario1 !=null){
            txtNombre.setText(usuario1.getNombre());
            txtApellido.setText(usuario1.getApellido());
            txtID.setText(""+usuario1.getId()+"");
            txtUsuario.setText(usuario1.getUsuario());
            txtClave.setText(usuario1.getClave());
            txtDireccion.setText(usuario1.getDireccionExacta());
            txtProvincia.setText(usuario1.getProvincia());
            txtCanton.setText(usuario1.getCanton());
            txtDistrito.setText(usuario1.getCanton());
            txtAvatar.setText(usuario1.getAvatar());
            txtFecha.setText(String.valueOf(usuario1.getFecha()));
            txtEdad.setText(""+usuario1.getEdad()+"");
            txtGenero.setText(usuario1.getGenero());
            txtCelular.setText(""+usuario1.getCelular()+"");
        }
    }



    public void registrarUsuario() throws Exception {
        mensaje = gestor.registrarUsuario(txtNombre.getText(),
                txtApellido.getText(),
                Integer.parseInt(txtID.getText()),
                txtUsuario.getText(),
                txtClave.getText(),
                txtDireccion.getText(),
                txtProvincia.getText(),
                txtCanton.getText(),
                txtDistrito.getText(),
                "Usuario",
                txtAvatar.getText(),
                LocalDate.parse(txtFecha.getText()),
                Integer.parseInt(txtEdad.getText()),
                txtGenero.getText(),
                Integer.parseInt(txtCelular.getText()));
        cargarListaUsuarios();
        mostrarMensaje(mensaje);

    }

    public void actualizarUsuario() throws Exception{
        mensaje = gestor.actualizarUsuario(txtNombre.getText(),
                txtApellido.getText(),
                Integer.parseInt(txtID.getText()),
                txtUsuario.getText(),
                txtClave.getText(),
                txtDireccion.getText(),
                txtProvincia.getText(),
                txtCanton.getText(),
                txtDistrito.getText(),
                "Usuario",
                txtAvatar.getText(),
                LocalDate.parse(txtFecha.getText()),
                Integer.parseInt(txtEdad.getText()),
                txtGenero.getText(),
                Integer.parseInt(txtCelular.getText()));
        cargarListaUsuarios();
        mostrarMensaje(mensaje);

    }



    public void eliminarUsuario()throws Exception{
        mensaje = gestor.eliminarUsuario(Integer.parseInt(txtID.getText()));
        cargarListaUsuarios();
        mostrarMensaje(mensaje);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            cargarListaUsuarios();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }


    @FXML
    void SALIR(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @FXML
    private void btnMetodo(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Pago.fxml"));
        Parent parentScene = loader.load();
        PagoController controller = loader.getController();
        controller.setPagoSesion(usuario1);
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }


    @FXML
    private void redireccionAuto(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Auto.fxml"));
        root = loader.load();
        AutoController autoController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> autoController.closeWindows());
    }


    @FXML
    private void redireccionMoto(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Moto.fxml"));
        root = loader.load();
        MotoController motoController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> motoController.closeWindows());
    }

    @FXML
    private void btnServicios(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Servicio.fxml"));
        root = loader.load();
        ServicioController servicioController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> servicioController.closeWindows());
    }

    @FXML
    private void Pedido(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Pedido.fxml"));
        root = loader.load();
        PedidoController pedidoController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> pedidoController.closeWindows());
    }


   
    
    
    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }

    public void closeWindows() {
    }



}
