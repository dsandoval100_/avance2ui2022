package sandoval.daniel.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController {

    private Stage stage;
    private Scene scene;
    private Parent root;


        @FXML
        private Button btnAdmin;

        @FXML
        private Button btnCliente;

        @FXML
        void Admin(ActionEvent event) throws IOException{
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Administrador.fxml"));
                root = loader.load();
                AdministradorController administradorController = loader.getController();
                stage =(Stage)((Node)event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                stage.setOnCloseRequest(e -> administradorController.closeWindows());
            }



        @FXML
        void Cliente(ActionEvent event) throws IOException {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Usuario.fxml"));
                root = loader.load();
                UsuarioController usuarioController = loader.getController();
                stage =(Stage)((Node)event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                stage.setOnCloseRequest(e -> usuarioController.closeWindows());
            }

    @FXML
    void InicioSesionUsuario(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/LoginUsuario.fxml"));
        root = loader.load();
        LoginUsuarioController loginUsuarioController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> loginUsuarioController.closeWindows());
    }

    @FXML
    void InicioSesionAdministrador(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/LoginUsuario.fxml"));
        root = loader.load();
        LoginUsuarioController loginUsuarioController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> loginUsuarioController.closeWindows());
    }

    @FXML
    void InicioSesionConductor(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/LoginUsuario.fxml"));
        root = loader.load();
        LoginUsuarioController loginUsuarioController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> loginUsuarioController.closeWindows());
    }



}


