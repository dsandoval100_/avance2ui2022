package sandoval.daniel.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import sandoval.daniel.bl.entities.Servicio.Servicio;
import sandoval.daniel.bl.logic.ServicioGestor;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ServicioController implements Initializable {

    private Servicio usuarioSesion;
    private String mensaje;
    private ServicioGestor gestor = new ServicioGestor();
    ObservableList<Object> servicioDAO = FXCollections.observableArrayList();

    @FXML
    private Button btnInicio;

    @FXML
    private Button btnSalir;

    @FXML
    private TableColumn colCod;

    @FXML
    private TableColumn colDes;

    @FXML
    private TableColumn colEst;

    @FXML
    private TableColumn colPre;

    @FXML
    private TableColumn colTipo;

    @FXML
    private TableView tblServicio;

    @FXML
    private TextField txtCodigo;

    @FXML
    private TextField txtDescripcion;

    @FXML
    private TextField txtEstado;

    @FXML
    private TextField txtPrecio;

    @FXML
    private TextField txtTipo;


    @FXML
    void SALIR(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Administrador.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            cargarListaServicios();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @FXML
    public void cargarListaServicios() throws Exception{
        tblServicio.getItems().clear();
        gestor.listarServicio().forEach(Servicio -> servicioDAO.add(Servicio));

        colCod.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        colDes.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        colPre.setCellValueFactory(new PropertyValueFactory<>("precio"));
        colTipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        colEst.setCellValueFactory(new PropertyValueFactory<>("estado"));

        tblServicio.setItems(servicioDAO);

    }

    public void cargarServicio(){
        Servicio servicio = (Servicio) tblServicio.getSelectionModel().getSelectedItem();
        if(servicio !=null){

            txtCodigo.setText(""+servicio.getCodigo()+"");
            txtDescripcion.setText(servicio.getDescripcion());
            txtPrecio.setText(servicio.getPrecio());
            txtTipo.setText(servicio.getTipo());
            txtEstado.setText(servicio.getEstado());

        }
    }



    public void registrarServicio() throws Exception {
        mensaje = gestor.registrarServicio(
                Integer.parseInt(txtCodigo.getText()),
                txtDescripcion.getText(), txtPrecio.getText(), txtTipo.getText(), txtEstado.getText());
        cargarListaServicios();
        mostrarMensaje(mensaje);

    }



    public void actualizarServicio()throws Exception {
        mensaje = gestor.actualizarServicio(
                Integer.parseInt(txtCodigo.getText()),
                txtDescripcion.getText(), txtPrecio.getText(), txtTipo.getText(), txtEstado.getText());
        cargarListaServicios();
        mostrarMensaje(mensaje);

    }


    public void eliminarServicio() throws Exception {
        mensaje = gestor.eliminarServicio(Integer.parseInt(txtCodigo.getText()));
        cargarListaServicios();
        mostrarMensaje(mensaje);
    }

    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }


    public void closeWindows() {
    }
}
