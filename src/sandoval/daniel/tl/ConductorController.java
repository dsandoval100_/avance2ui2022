package sandoval.daniel.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sandoval.daniel.bl.entities.Conductor.Conductor;
import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.bl.logic.ConductorGestor;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ConductorController implements Initializable {

    private Conductor usuarioSesion;
    private String mensaje;
    private ConductorGestor gestor = new ConductorGestor();
    ObservableList<Object> conductorDAO = FXCollections.observableArrayList();


    @FXML
    private Button btnActualizarConductor;

    @FXML
    private Button btnEliminarConductor;

    @FXML
    private Button btnInicio;

    @FXML
    private Button btnRegistrarConductor;

    @FXML
    private Button btnSalir;

    @FXML
    private TableColumn colApe;

    @FXML
    private TableColumn colAva;

    @FXML
    private TableColumn colCla;

    @FXML
    private TableColumn  colDir;

    @FXML
    private TableColumn colEdad;

    @FXML
    private TableColumn colEstado;

    @FXML
    private TableColumn colFecha;

    @FXML
    private TableColumn colId;

    @FXML
    private TableColumn colNom;

    @FXML
    private TableColumn colUsua;

    @FXML
    private TableColumn colProv;

    @FXML
    private TableColumn colDist;

    @FXML
    private TableColumn colCan;


    @FXML
    private Label lblTitulo;

    @FXML
    private TableView tblConductor;

    @FXML
    private TextField txtApellido;

    @FXML
    private TextField txtAvatar;

    @FXML
    private TextField txtClave;

    @FXML
    private TextField txtDireccion;

    @FXML
    private TextField txtEdad;

    @FXML
    private TextField txtEstado;

    @FXML
    private TextField txtFecha;

    @FXML
    private TextField txtID;

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtUsuario;

    @FXML
    private TextField txtCanton;

    @FXML
    private TextField txtDistrito;

    @FXML
    private TextField txtProvincia;

    private Node cerrar;

    private Persona usuarioLogueado;


    public void setUsuarioLogueado(Persona persona) { // Setting the client-object in ClientViewController
        this.usuarioLogueado = persona;
        setTitulo();
    }

    private void setTitulo(){
        lblTitulo.setText("Bienvenido " + usuarioLogueado.getId() + " - " + usuarioLogueado.getNombre());
    }


    @FXML
    public void cargarListaConductores() throws Exception{

        tblConductor.getItems().clear();
        gestor.listarConductor().forEach(Conductor -> conductorDAO.add(Conductor));

        colNom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colApe.setCellValueFactory(new PropertyValueFactory<>("apellido"));
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colUsua.setCellValueFactory(new PropertyValueFactory<>("usuario"));
        colCla.setCellValueFactory(new PropertyValueFactory<>("clave"));
        colDir.setCellValueFactory(new PropertyValueFactory<>("direccionExacta"));
        colProv.setCellValueFactory(new PropertyValueFactory<>("provincia"));
        colCan.setCellValueFactory(new PropertyValueFactory<>("canton"));
        colDist.setCellValueFactory(new PropertyValueFactory<>("distrito"));
        colAva.setCellValueFactory(new PropertyValueFactory<>("avatar"));
        colFecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
        colEdad.setCellValueFactory(new PropertyValueFactory<>("edad"));
        colEstado.setCellValueFactory(new PropertyValueFactory<>("estado"));

        tblConductor.setItems(conductorDAO);

    }

    public void cargarConductor(){
        Conductor usuario = (Conductor) tblConductor.getSelectionModel().getSelectedItem();
        if(usuario !=null){
            txtNombre.setText(usuario.getNombre());
            txtApellido.setText(usuario.getApellido());
            txtID.setText(""+usuario.getId()+"");
            txtUsuario.setText(usuario.getUsuario());
            txtClave.setText(usuario.getClave());
            txtDireccion.setText(usuario.getDireccionExacta());
            txtProvincia.setText(usuario.getProvincia());
            txtCanton.setText(usuario.getCanton());
            txtDistrito.setText(usuario.getCanton());
            txtAvatar.setText(usuario.getAvatar());
            txtFecha.setText(String.valueOf(usuario.getFecha()));
            txtEdad.setText(""+usuario.getEdad()+"");
            txtEstado.setText(usuario.getEstado());
        }
    }


    public void registrarConductor() throws Exception {
        mensaje = gestor.registrarConductor(txtNombre.getText(),
                txtApellido.getText(),
                Integer.parseInt(txtID.getText()),
                txtUsuario.getText(),
                txtClave.getText(),
                txtDireccion.getText(),
                txtProvincia.getText(),
                txtCanton.getText(),
                txtDistrito.getText(),
                "Conductor",
                txtAvatar.getText(),
                LocalDate.parse(txtFecha.getText()),
                Integer.parseInt(txtEdad.getText()),
                txtEstado.getText());
        cargarListaConductores();
        mostrarMensaje(mensaje);

    }


    public void actualizarConductor() throws Exception{
        mensaje = gestor.actualizarConductor(txtNombre.getText(),
                txtApellido.getText(),
                Integer.parseInt(txtID.getText()),
                txtUsuario.getText(),
                txtClave.getText(),
                txtDireccion.getText(),
                txtProvincia.getText(),
                txtCanton.getText(),
                txtDistrito.getText(),
                "Conductor",
                txtAvatar.getText(),
                LocalDate.parse(txtFecha.getText()),
                Integer.parseInt(txtEdad.getText()),
                txtEstado.getText());
        cargarListaConductores();
        mostrarMensaje(mensaje);

    }



    public void eliminarConductor()throws Exception{
        mensaje = gestor.eliminarConductor(Integer.parseInt(txtID.getText()));
        cargarListaConductores();
        mostrarMensaje(mensaje);
    }





    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Administrador.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @FXML
    void SALIR(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    public void closeWindows()  {

    }

    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            cargarListaConductores();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
}
