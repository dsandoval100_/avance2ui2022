package sandoval.daniel.tl;

        import javafx.collections.FXCollections;
        import javafx.collections.ObservableList;
        import javafx.event.ActionEvent;
        import javafx.fxml.FXML;
        import javafx.fxml.FXMLLoader;
        import javafx.fxml.Initializable;
        import javafx.scene.Parent;
        import javafx.scene.Scene;
        import javafx.scene.control.*;
        import javafx.scene.control.cell.PropertyValueFactory;
        import javafx.stage.Stage;
        import sandoval.daniel.bl.entities.Administrador.Administrador;
        import sandoval.daniel.bl.entities.Pago.Pago;
        import sandoval.daniel.bl.entities.Usuario.Usuario;
        import sandoval.daniel.bl.logic.PagoGestor;
        import sandoval.daniel.bl.logic.UsuarioGestor;

        import java.io.IOException;
        import java.net.URL;
        import java.util.ResourceBundle;

public class PagoController implements Initializable {

    private Usuario PagoSesion;
    private String mensaje;
    private PagoGestor gestor = new PagoGestor();
    ObservableList<Object> pagoDAO = FXCollections.observableArrayList();

    @FXML
    private Button btnActualizarMetodo;

    @FXML
    private Button btnEliminarMetodo;

    @FXML
    private Button btnRegistrarMetodo;



    @FXML
    private TableColumn colNum;

    @FXML
    private TableColumn colPro;

    @FXML
    private TableColumn colID;



    @FXML
    private TableView tblPago;

    @FXML
    private TextField txtNumero;

    @FXML
    private TextField txtProveedor;

    @FXML
    private TextField txtID;


    public Usuario getPagoSesion() {
        return PagoSesion;
    }

    public void setPagoSesion(Usuario PagoSesion) {
        this.PagoSesion = PagoSesion;


    }




    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Usuario.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @FXML
    void SALIR(ActionEvent event) throws IOException {

        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    public void initialize(URL location, ResourceBundle resources) {
        try {
            cargarListaPago();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @FXML
    public void cargarListaPago() throws Exception{


        tblPago.getItems().clear();
        gestor.listarPago().forEach(Pago -> pagoDAO.add(Pago));

        colNum.setCellValueFactory(new PropertyValueFactory<>("numero"));
        colPro.setCellValueFactory(new PropertyValueFactory<>("proveedor"));
        colID.setCellValueFactory(new PropertyValueFactory<>("id"));



        tblPago.setItems(pagoDAO);

    }

    public void cargarPago(){
        Pago pago = (Pago) tblPago.getSelectionModel().getSelectedItem();
        if(pago !=null){

            txtNumero.setText(""+pago.getNumero()+"");
            txtProveedor.setText(""+pago.getProveedor()+"");
            txtID.setText(""+pago.getId()+"");

        }
    }

    public void registrarPago() throws Exception {
        mensaje = gestor.registrarPago(
                Integer.parseInt(txtNumero.getText()),
                txtProveedor.getText(), Integer.parseInt(txtID.getText()));
        cargarListaPago();
        mostrarMensaje(mensaje);

    }

    public void actualizarPago() throws Exception {
        mensaje = gestor.actualizarPago(
                Integer.parseInt(txtNumero.getText()),
                txtProveedor.getText(),Integer.parseInt(txtID.getText()));
        cargarListaPago();
        mostrarMensaje(mensaje);

    }

    public void eliminarPago()throws Exception{
        mensaje = gestor.eliminarPago(Integer.parseInt(txtNumero.getText()));
        cargarListaPago();
        mostrarMensaje(mensaje);
    }

    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }


}
