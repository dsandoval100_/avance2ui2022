package sandoval.daniel.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.bl.logic.AdministradorGestor;
import sandoval.daniel.bl.logic.PersonaGestor;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AdministradorController implements Initializable {
    private Stage stage;
    private Scene scene;
    private Parent root;

    private Administrador usuarioSesion;
    private String mensaje;
    private AdministradorGestor gestor = new AdministradorGestor();
    private PersonaGestor gestorAdmin = new PersonaGestor();
    ObservableList<Object> administradorDAO = FXCollections.observableArrayList();

    @FXML
    private Button btnConductor;

    @FXML
    private Button btnInicio;

    @FXML
    private Button btnRegistrar;

    @FXML
    private Button btnSalir;

    @FXML
    private TableView tblAdministrador;


    @FXML
    private TableColumn colNom;

    @FXML
    private TableColumn colApe;

    @FXML
    private TableColumn colId;

    @FXML
    private TableColumn colUsua;


    @FXML
    private TableColumn colCla;

    @FXML
    private TableColumn colDir;



    @FXML
    private TableColumn colProv;


    @FXML
    private TableColumn colCan;


    @FXML
    private TableColumn colDist;



    @FXML
    private Label lblTitulo;



    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtApellido;

    @FXML
    private TextField txtID;
    @FXML
    private TextField txtUsuario;


    @FXML
    private TextField txtClave;

    @FXML
    private TextField txtDireccion;

    @FXML
    private TextField txtProvincia;


    @FXML
    private TextField txtCanton;

    @FXML
    private TextField txtDistrito;

    private Persona usuarioLogueado;


    public void setUsuarioLogueado(Persona persona) { // Setting the client-object in ClientViewController
        this.usuarioLogueado = persona;
        setTitulo();
    }

    private void setTitulo(){
        lblTitulo.setText("Bienvenido " + usuarioLogueado.getId() + " - " + usuarioLogueado.getNombre());
    }

    @FXML
    void redireccionConductor(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Conductor.fxml"));
        root = loader.load();
        ConductorController conductorController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> conductorController.closeWindows());
    }

    @FXML
    void redireccionServicios(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sandoval/daniel/ui/Servicio.fxml"));
        root = loader.load();
        ServicioController servicioController = loader.getController();
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(e -> servicioController.closeWindows());
    }

    public void closeWindows() {
    }


    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    @FXML
    void SALIR(ActionEvent event) throws IOException {

        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }

    public void initialize(URL location, ResourceBundle resources) {
        try {
            cargarListaAdministradores();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }



    @FXML
    public void cargarListaAdministradores() throws Exception{

        tblAdministrador.getItems().clear();

        /**stage =(Stage)tblAdministrador.getScene().getWindow();
        Persona personita = (Persona) stage.getUserData();
        gestorAdmin.consultarAdministrador(personita.getId()).forEach(Administrador -> administradorDAO.add(Administrador));*/

        gestor.listarAdministradores().forEach(Administrador -> administradorDAO.add(Administrador));

        colNom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colApe.setCellValueFactory(new PropertyValueFactory<>("apellido"));
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colUsua.setCellValueFactory(new PropertyValueFactory<>("usuario"));
        colCla.setCellValueFactory(new PropertyValueFactory<>("clave"));
        colDir.setCellValueFactory(new PropertyValueFactory<>("direccionExacta"));
        colProv.setCellValueFactory(new PropertyValueFactory<>("provincia"));
        colCan.setCellValueFactory(new PropertyValueFactory<>("canton"));
        colDist.setCellValueFactory(new PropertyValueFactory<>("distrito"));


        tblAdministrador.setItems(administradorDAO);

    }

    public void cargarAdministrador(){
        Administrador usuario = (Administrador) tblAdministrador.getSelectionModel().getSelectedItem();
        if(usuario !=null){
            txtNombre.setText(usuario.getNombre());
            txtApellido.setText(usuario.getApellido());
            txtID.setText(""+usuario.getId()+"");
            txtUsuario.setText(usuario.getUsuario());
            txtClave.setText(usuario.getClave());
            txtDireccion.setText(usuario.getDireccionExacta());
            txtProvincia.setText(usuario.getProvincia());
            txtCanton.setText(usuario.getCanton());
            txtDistrito.setText(usuario.getDistrito());

        }

    }

    public void registrarAdministrador() throws Exception {
        mensaje = gestor.registrarAdministrador(txtNombre.getText(),
                txtApellido.getText(),
                Integer.parseInt(txtID.getText()),
                txtUsuario.getText(),
                txtClave.getText(),
                txtDireccion.getText(),
                txtProvincia.getText(),
                txtCanton.getText(),
                txtDistrito.getText(),
                "Administrador");
        cargarListaAdministradores();
        mostrarMensaje(mensaje);

    }


    public void actualizarAdministrador() throws Exception{
        mensaje = gestor.actualizarAdministrador(txtNombre.getText(),
                txtApellido.getText(),
                Integer.parseInt(txtID.getText()),
                txtUsuario.getText(),
                txtClave.getText(),
                txtDireccion.getText(),
                txtProvincia.getText(),
                txtCanton.getText(),
                txtDistrito.getText(),
                "Adminitrador");
        cargarListaAdministradores();
        mostrarMensaje(mensaje);

    }



    public void eliminarAdministrador()throws Exception{
        mensaje = gestor.eliminarAdministrador(Integer.parseInt(txtID.getText()));
        cargarListaAdministradores();
        mostrarMensaje(mensaje);
    }




    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }



}
