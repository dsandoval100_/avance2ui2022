package sandoval.daniel.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Auto.Auto;
import sandoval.daniel.bl.logic.AutoGestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AutoController implements Initializable {

    private Auto usuarioSesion;
    private String mensaje;
    private AutoGestor gestor = new AutoGestor();
    ObservableList<Object> autoDAO = FXCollections.observableArrayList();


    @FXML
    private Button btnActualizarAuto;

    @FXML
    private Button btnEliminarAuto;

    @FXML
    private Button btnInicio;

    @FXML
    private Button btnRegistrarAuto;

    @FXML
    private Button btnSalir;

    @FXML
    private TableColumn colAnioAuto;

    @FXML
    private TableColumn colColAuto;

    @FXML
    private TableColumn colMarAuto;

    @FXML
    private TableColumn colPlaAuto;

    @FXML
    private TableColumn colTipAuto;

    @FXML
    private TableView tblAuto;

    @FXML
    private TextField txtAnioAuto;

    @FXML
    private TextField txtColorAuto;

    @FXML
    private TextField txtMarcaAuto;

    @FXML
    private TextField txtPlacaAuto;

    @FXML
    private TextField txtTipoAuto;



    @FXML
    void SALIR(ActionEvent event) throws IOException {

        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Menu.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();

    }
    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("/sandoval/daniel/ui/Usuario.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();


    }

    public void initialize(URL location, ResourceBundle resources) {
        try {
            cargarListaAutos();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void cargarListaAutos() throws Exception{
        tblAuto.getItems().clear();
        gestor.listarAuto().forEach(Auto -> autoDAO.add(Auto));

        colMarAuto.setCellValueFactory(new PropertyValueFactory<>("marca"));
        colPlaAuto.setCellValueFactory(new PropertyValueFactory<>("placa"));
        colTipAuto.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        colColAuto.setCellValueFactory(new PropertyValueFactory<>("color"));
        colAnioAuto.setCellValueFactory(new PropertyValueFactory<>("anio"));

        tblAuto.setItems(autoDAO);

    }

    public void cargarAuto(){
        Auto usuario = (Auto) tblAuto.getSelectionModel().getSelectedItem();
        if(usuario !=null){
            txtMarcaAuto.setText(usuario.getMarca());
            txtPlacaAuto.setText(usuario.getPlaca());
            txtTipoAuto.setText(usuario.getTipo());
            txtColorAuto.setText(usuario.getColor());
            txtAnioAuto.setText(usuario.getAnio());
        }

    }





    public void registrarAuto() throws Exception {
        mensaje = gestor.registrarAuto(txtMarcaAuto.getText(),
                txtPlacaAuto.getText(),txtTipoAuto.getText(),
                txtColorAuto.getText(),txtAnioAuto.getText());
        cargarListaAutos();
        mostrarMensaje(mensaje);


    }



    public void actualizarAuto() throws Exception  {
        mensaje = gestor.actualizarAuto(txtMarcaAuto.getText(),
                txtPlacaAuto.getText(),txtTipoAuto.getText(),
                txtColorAuto.getText(),txtAnioAuto.getText());
        cargarListaAutos();
        mostrarMensaje(mensaje);
    }


    public void eliminarAuto() throws Exception {
        mensaje = gestor.eliminarAuto(txtPlacaAuto.getText());
        cargarListaAutos();
        mostrarMensaje(mensaje);

    }



    private void mostrarMensaje(String mensaje){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje);
        alert.setHeaderText(null);
        alert.showAndWait();
    }

    public void closeWindows() {
    }
}
